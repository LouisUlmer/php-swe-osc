<?php
/**
 * Created by PhpStorm.
 * User: Louis
 * Date: 02.07.2018
 * Time: 13:26
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/php/AutoloaderDB.php';
$events = $eventModel->getAllIDS();
?>
<!DOCTYPE HTML>
<!--
    Miniport by HTML5 UP
    html5up.net | @ajlkn
    Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<head>
    <title>OSC Schissen</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!--[if lte IE 8]>
    <script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ie8.css"/><![endif]-->
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="assets/css/ie9.css"/><![endif]-->
</head>
<body>

<!-- Nav -->
<nav id="nav">
    <ul class="container">
        <li><a href="/">Home</a></li>
        <li><a href="http://www.osc-bremerhaven.de/">OSC</a></li>
        <li><a href="./event.php">Events</a></li>
        <?php if (isset($user)) { ?>
            <li><a href="./mitgliederliste.php">Mitgliederliste</a></li>
        <?php } ?>
        <?php if (isset($user)) { ?>
            <li><a href="./essen.php">Essen</a></li>
            <li><a href="./lager.php">Lager</a></li>
        <?php } ?>
        <li>
            <a href="./<?php echo isset($user) ? "logout" : "login" ?>.php"><?php echo isset($user) ? "Logout" : "Login" ?></a>
        </li>
    </ul>
</nav>

<!-- Contact -->
<div class="wrapper style2">
    <article id="contact" class="container 120%">
        <header>
            <h2>Essen</h2>
        </header>
        <script type="text/javascript">
            function check(elem) {
                document.getElementById('event').disabled = !elem.selectedIndex;
                if (document.getElementById('event').disabled) {
                    document.getElementById('event').style.backgroundColor = "lightgray";
                    document.getElementById('event')[0].selected = 'selected';
                } else {
                    document.getElementById('event').style.backgroundColor = "white";
                }
            }

            function calculate_total() {
                var els = document.getElementsByClassName('Assessment');
                var total1 = 0.0;
                for (var i = 0; i < els.length; i++) {
                    total1 += parseFloat(els[i].value);
                }
                document.getElementById('ScoreTot').value = 'Gesamt: ' + total1.toFixed(2).toLocaleString() + ' €';
                document.getElementById('ScoreTot').style.color = 'black';

            }
        </script>
        <div>
            <form action="/" method="post" id="cancle"></form>
            <form action="/essenbs.php" method="post" id="continue"></form>
            <div class="row">
                <div class="6u 12u(mobile)">
                    <form>
                        <section class="box style1">

                            <label>Veranstaltung auswählen
                                <select id="ver" form="continue" onChange="check(this);" style="color: black;">
                                    <option>
                                        Trainingstage
                                    </option>
                                    <option>
                                        Event
                                    </option>
                                </select></label>
                            <label>Event auswählen
                                <select id="event" form="continue" disabled="disabled"
                                        style="background-color: lightgray; color: gray;">
                                    <option>
                                        ---
                                    </option>
                                    <?php for ($i = 0; $i < count($events); ++$i) { ?>
                                        <option>
                                            <?php echo $eventModel->getEvent($events[$i]["EventID"])["name"] . " am : " . (new DateTime($eventModel->getEvent($events[$i]["EventID"])["start"]))->format('d M Y'); ?>
                                        </option>
                                    <?php } ?>
                                </select></label>
                            <label>Uhrzeit auswählen
                                <select form="continue" style="color: black;">
                                    <option>
                                        18:00-18:30
                                    </option>
                                    <option>
                                        18:30-19:00
                                    </option>
                                    <option>
                                        19:00-19:30
                                    </option>
                                    <option>
                                        19:30-20:00
                                    </option>
                                    <option>
                                        20:00-20:30
                                    </option>
                                    <option>
                                        20:30-21:00
                                    </option>
                                    <option>
                                        21:00-21:30
                                    </option>
                                </select></label>
                            <div class="12u 12u(mobile)">
                                <input type="text" name="ScoreTot" id="ScoreTot" placeholder="Gesamt: 0,00€" disabled form="continue"/>
                            </div>

                            </br>
                            </br>
                            <input type="submit" placeholder="Weiter" value="Weiter" form="continue">
                            <input type="submit" placeholder="Abrechen" value="Abrechen" form="cancle">

                        </section>
                        </br>
                    </form>
                </div>
                <div class="6u 12u(mobile)">
                    <section class="box style1">
                        <form>
                            <label>Gericht auswählen
                                <select style="color: black;" name="hs" form="continue" class="Assessment"
                                        onChange="calculate_total();">
                                    <option value="0.0">
                                        --
                                    </option>
                                    <option value="7.40" name="3">
                                        Schnitzel, Pommes  +7,40€
                                    </option>
                                    <option value="7.40">
                                        Currywurst, Pommes  +7,40€
                                    </option>
                                    <option value="7.40">
                                        1/2 Hähnchen, Pommes  +7,40€
                                    </option>
                                </select></label>
                            <label>Sauce auswählen
                                <select style="color: black;" form="continue" name="suce" class="Assessment"
                                        onChange="calculate_total();">
                                    <option value="0.0">
                                        --
                                    </option>
                                    <option value="0.0">
                                        Hausdressing  +0,00€
                                    </option>
                                    <option value="0.0">
                                        Ketchup  +0,00€
                                    </option>
                                    <option value="0.0">
                                        Mayonnaise  +0,00€
                                    </option>
                                </select></label>

                            <label>Beilage auswählen
                                <select style="color: black;" form="continue" name="ex" class="Assessment"
                                        onChange="calculate_total();">
                                    <option value="0.0">
                                        --
                                    </option>
                                    <option value="3.5">
                                        Rohkost-Salat  +3,50€
                                    </option>
                                    <option value="3.5">
                                        Reis  +3,50€
                                    </option>
                                    <option value="3.5">
                                        Gemüseaufschnitt  +3,50€
                                    </option>
                                </select></label>

                            <label>Getränke auswählen
                                <select style="color: black;" form="continue" name="drink" class="Assessment"
                                        onChange="calculate_total();">
                                    <option value="0.0">
                                        --
                                    </option>
                                    <option value="3.1">
                                        Bier 0,5l  +3,10€
                                    </option>
                                    <option value="3.1">
                                        Cola, Fanta, Sprite 0,5l  +3,10€
                                    </option>
                                    <option value="3.1">
                                        Wasser 0,75l  +3,10€
                                    </option>
                                </select></label>
                            <label>Zahlungsart auswählen
                                <select style="color: black;" form="continue">
                                    <option>
                                        Barzahlung (vor Ort)
                                    </option>
                                    <option>
                                        Online-Bezahlung (via PayPal)
                                    </option>
                                </select></label>
                        </form>
                    </section>
                </div>
            </div>
            <div class="row">
                <div class="12u">
                    <hr/>
                    <h3>Find me on ...</h3>
                    <ul class="social">
                        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                        <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
                        <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
                        <li><a href="#" class="icon fa-tumblr"><span class="label">Tumblr</span></a></li>
                        <li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
                        <li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
                        <!--
                        <li><a href="#" class="icon fa-rss"><span>RSS</span></a></li>
                        <li><a href="#" class="icon fa-instagram"><span>Instagram</span></a></li>
                        <li><a href="#" class="icon fa-foursquare"><span>Foursquare</span></a></li>
                        <li><a href="#" class="icon fa-skype"><span>Skype</span></a></li>
                        <li><a href="#" class="icon fa-soundcloud"><span>Soundcloud</span></a></li>
                        <li><a href="#" class="icon fa-youtube"><span>YouTube</span></a></li>
                        <li><a href="#" class="icon fa-blogger"><span>Blogger</span></a></li>
                        <li><a href="#" class="icon fa-flickr"><span>Flickr</span></a></li>
                        <li><a href="#" class="icon fa-vimeo"><span>Vimeo</span></a></li>
                        -->
                    </ul>
                    <hr/>
                </div>
            </div>
        </div>
        <footer>
            <ul id="copyright">
                <li>&copy; Untitled. All rights reserved.</li>
                <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
            </ul>
        </footer>
    </article>
</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.scrolly.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/skel-viewport.min.js"></script>
<script src="assets/js/util.js"></script>
<!--[if lte IE 8]>
<script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/main.js"></script>

</body>
</html>
