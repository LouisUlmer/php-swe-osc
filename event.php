<?php
/**
 * Created by PhpStorm.
 * User: Louis
 * Date: 30.06.2018
 * Time: 19:59
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/php/AutoloaderDB.php';
$events = $eventModel->getAllIDS();
$ids = [];
for ($i = 0; $i < count($events); ++$i)
    array_push($ids, $events[$i]["EventID"]);

$event = $eventModel->getEvent(isset($_GET["id"]) ? isset($_GET["join"]) ? $_GET["join"] : $_GET["id"] : $events[0]["EventID"]);

if (($key = array_search($event["EventID"], $ids)) !== false) {
    unset($ids[$key]);
}

if (isset($_GET["join"])) {
    if (isset($user)) {
        $eventModel->join($user["ID"], $_GET["join"], 1);
    } else {
        header('Location: /login.html');
        exit();
    }
}
?>


<!DOCTYPE HTML>
<!--
    Miniport by HTML5 UP
    html5up.net | @ajlkn
    Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<head>
    <title>OSC Schissen</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!--[if lte IE 8]>
    <script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ie8.css"/><![endif]-->
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="assets/css/ie9.css"/><![endif]-->
</head>
<body>

<!-- Nav -->
<nav id="nav">
    <ul class="container">
        <li><a href="/">Home</a></li>
        <li><a href="http://www.osc-bremerhaven.de/">OSC</a></li>
        <li><a href="./event.php">Events</a></li>
        <?php if (isset($user)) { ?>
            <li><a href="./mitgliederliste.php">Mitgliederliste</a></li>
        <?php } ?>
        <?php if (isset($user)) { ?>
            <li><a href="./essen.php">Essen</a></li>
            <li><a href="./lager.php">Lager</a></li>
        <?php } ?>
        <li>
            <a href="./<?php echo isset($user) ? "logout" : "login" ?>.php"><?php echo isset($user) ? "Logout" : "Login" ?></a>
        </li>
    </ul>
</nav>

<!-- Contact -->
<div class="wrapper style2">
    <article id="contact" class="container 120%">
        <header>
            <h2>Events</h2>
        </header>
        <div>
            <div class="row">
                <div class="6u 12u(mobile)">
                    <article class="box style2">
                        <h3>
                            <a href="/event.php?id=<?php echo $event["EventID"] ?> "><?php echo $event["name"]; ?> </a>
                        </h3>
                        <p><?php echo $event["description"]; ?></p>
                        </br>
                        <p><?php echo $eventModel->getEventParticipantCount($event["EventID"])["participantCount"]; ?>
                            /<?php echo $event["maxparticipant"]; ?> Teilenhmer </p>
                        <div class="row 200%">
                            <div class="12u">
                                <ul class="actions">
                                    <a class="button"
                                       href="event.php?join=<?php echo $event["EventID"]; ?>">Teilenhemen</a>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="6u 12u(mobile)">
                    <?php for ($i = 0;
                               $i <= count($ids);
                               ++$i) {
                        if (isset($ids[$i])) {
                            ?>

                            <section class="box style1">
                                <a href="event.php?id=<?php echo $eventModel->getEvent($ids[$i])["EventID"]; ?>">
                                    <h3><?php echo $eventModel->getEvent($ids[$i])["name"]; ?></h3></a>
                                <p><?php echo mb_strimwidth($eventModel->getEvent($ids[$i])["description"], 0, 255) . "..."; ?> </p>
                            </section>
                        <?php }
                    } ?>
                </div>
            </div>
            <div class="row">
                <div class="12u">
                    <hr/>
                    <h3>Find me on ...</h3>
                    <ul class="social">
                        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                        <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
                        <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
                        <li><a href="#" class="icon fa-tumblr"><span class="label">Tumblr</span></a></li>
                        <li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
                        <li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
                        <!--
                        <li><a href="#" class="icon fa-rss"><span>RSS</span></a></li>
                        <li><a href="#" class="icon fa-instagram"><span>Instagram</span></a></li>
                        <li><a href="#" class="icon fa-foursquare"><span>Foursquare</span></a></li>
                        <li><a href="#" class="icon fa-skype"><span>Skype</span></a></li>
                        <li><a href="#" class="icon fa-soundcloud"><span>Soundcloud</span></a></li>
                        <li><a href="#" class="icon fa-youtube"><span>YouTube</span></a></li>
                        <li><a href="#" class="icon fa-blogger"><span>Blogger</span></a></li>
                        <li><a href="#" class="icon fa-flickr"><span>Flickr</span></a></li>
                        <li><a href="#" class="icon fa-vimeo"><span>Vimeo</span></a></li>
                        -->
                    </ul>
                    <hr/>
                </div>
            </div>
        </div>
        <footer>
            <ul id="copyright">
                <li>&copy; Untitled. All rights reserved.</li>
                <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
            </ul>
        </footer>
    </article>
</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.scrolly.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/skel-viewport.min.js"></script>
<script src="assets/js/util.js"></script>
<!--[if lte IE 8]>
<script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/main.js"></script>

</body>
</html>
