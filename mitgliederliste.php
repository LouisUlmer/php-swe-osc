<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/php/AutoloaderDB.php'; ?>
<!DOCTYPE HTML>
<!--
	Miniport by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<head>
    <title>OSC Schissen</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!--[if lte IE 8]>
    <script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ie8.css"/><![endif]-->
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="assets/css/ie9.css"/><![endif]-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <style>
        .box {
            width: 1370px;
            background-color: #fff;
            border: 1px solid #ccc;
            border-radius: 5px;
            margin-top: 25px;
            box-sizing: border-box;
        }
    </style>
</head>
<body>

<!-- Nav -->
<nav id="nav">
    <ul class="container">
        <li><a href="/">Home</a></li>
        <li><a href="http://www.osc-bremerhaven.de/">OSC</a></li>
        <li><a href="./event.php">Events</a></li>
        <?php if (isset($user)) { ?>
            <li><a href="./mitgliederliste.php">Mitgliederliste</a></li>
        <?php } ?>
        <?php if (isset($user)) { ?>
            <li><a href="./essen.php">Essen</a></li>
            <li><a href="./lager.php">Lager</a></li>
        <?php } ?>
        <li>
            <a href="./<?php echo isset($user) ? "logout" : "login" ?>.php"><?php echo isset($user) ? "Logout" : "Login" ?></a>
        </li>
    </ul>
</nav>

<!-- Contact -->
<div class="wrapper style2">
    <article id="contact" class="container 135%">
        <header>
            <h2>Mitglieder Liste</h2>
        </header>
        <div class="container">
            <div class="row 200%">
                <div class="12u">

                    <div class="container box">
                        <h1 align="center">Mitgliederverwaltung  </h1>
                        <br/>
                        <div class="table-responsive">
                            <br/>
                            <div align="right">
                                <button type="button" onclick="fetch_data();" class="btn btn-info">Laden</button>
                                <button onclick="inser();" type="button" name="add" id="add" class="btn btn-info">
                                    Hinzufügen
                                </button>
                            </div>
                            <br/>
                            <div id="alert_message"></div>
                            <table id="user_data" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Mitarbeiter</th>
                                    <th>Name</th>
                                    <th>Vorname</th>
                                    <th>Alter</th>
                                    <th>Nutzername</th>
                                    <th>Anschrift</th>
                                    <th>Aktiv</th>
                                    <th></th>
                                </tr>


                                </thead>

                                <?php

                                $result = $userModel->getAll();
                                for ($i = 0; $i < count($result); ++$i) {
                                    echo '<tr><td><div contenteditable class="update" data-id="' . $result[$i]["ID"] . '" data-column="mitarbeiter">' . $result[$i]["mitarbeiter"] . '</div></td>';
                                    echo '<td><div contenteditable class="update" data-id="' . $result[$i]["ID"] . '" data-column="name">' . $result[$i]["name"] . '</div></td>';
                                    echo '<td><div contenteditable class="update" data-id="' . $result[$i]["ID"] . '" data-column="vorname">' . $result[$i]["vorname"] . '</div></td>';
                                    echo '<td><div contenteditable class="update" data-id="' . $result[$i]["ID"] . '" data-column="alter">' . $result[$i]["alter"] . '</div></td>';
                                    echo '<td><div contenteditable class="update" data-id="' . $result[$i]["ID"] . '" data-column="nutzername">' . $result[$i]["nutzername"] . '</div></td>';
                                    echo '<td><div contenteditable class="update" data-id="' . $result[$i]["ID"] . '" data-column="Anschrift">' . $result[$i]["Anschrift"] . '</div></td>';
                                    echo '<td><div contenteditable class="update" data-id="' . $result[$i]["ID"] . '" data-column="aktiv">' . $result[$i]["aktiv"] . '</div></td>';
                                    echo '<td><button type="button" name="delete" class="btn btn-danger btn-xs delete" id="' . $result[$i]["ID"] . '">Delete</button></td></tr>';

                                }

                                ?>


                            </table>
                        </div>
                    </div>
                </div>


            </div>
            <div class="row">
                <div class="12u">
                    <hr/>
                    <h3>Find me on ...</h3>
                    <ul class="social">
                        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                        <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
                        <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
                        <li><a href="#" class="icon fa-tumblr"><span class="label">Tumblr</span></a></li>
                        <li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
                        <li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
                        <!--
                        <li><a href="#" class="icon fa-rss"><span>RSS</span></a></li>
                        <li><a href="#" class="icon fa-instagram"><span>Instagram</span></a></li>
                        <li><a href="#" class="icon fa-foursquare"><span>Foursquare</span></a></li>
                        <li><a href="#" class="icon fa-skype"><span>Skype</span></a></li>
                        <li><a href="#" class="icon fa-soundcloud"><span>Soundcloud</span></a></li>
                        <li><a href="#" class="icon fa-youtube"><span>YouTube</span></a></li>
                        <li><a href="#" class="icon fa-blogger"><span>Blogger</span></a></li>
                        <li><a href="#" class="icon fa-flickr"><span>Flickr</span></a></li>
                        <li><a href="#" class="icon fa-vimeo"><span>Vimeo</span></a></li>
                        -->
                    </ul>
                    <hr/>
                </div>
            </div>
        </div>
        <footer>
            <ul id="copyright">
                <li>&copy; Untitled. All rights reserved.</li>
                <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
            </ul>
        </footer>
    </article>
</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.scrolly.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/skel-viewport.min.js"></script>
<script src="assets/js/util.js"></script>
<!--[if lte IE 8]>
<script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/main.js"></script>

</body>
</html>

<script type="text/javascript" language="javascript">
    $(document).ready(function () {

        fetch_data();

        function fetch_data() {
            var dataTable = $('#user_data').DataTable({
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    url: "/assets/php/fetch.php",
                    type: "POST"
                }
            });
        }

        function inser() {
            $('#add').click(function () {
                var html = '<tr>';
                html += '<td contenteditable id="data1"></td>';
                html += '<td contenteditable id="data2"></td>';
                html += '<td contenteditable id="data3"></td>';
                html += '<td contenteditable id="data4"></td>';
                html += '<td contenteditable id="data5"></td>';
                html += '<td contenteditable id="data6"></td>';
                html += '<td contenteditable id="data7"></td>';
                html += '<td><button type="button" name="insert" id="insert" class="btn btn-success btn-xs">Insert</button></td>';
                html += '</tr>';
                $('#user_data tbody').prepend(html);
            });

        }

        function update_data(id, column_name, value) {
            $.ajax({
                url: "/assets/php/update.php",
                method: "POST",
                data: {id: id, column_name: column_name, value: value},
                success: function (data) {
                    $('#alert_message').html('<div class="alert alert-success">' + data + '</div>');
                    $('#user_data').DataTable().destroy();
                    fetch_data();
                }
            });
            setInterval(function () {
                $('#alert_message').html('');
            }, 5000);
        }

        $(document).on('blur', '.update', function () {
            var id = $(this).data("id");
            var column_name = $(this).data("column");
            var value = $(this).text();
            update_data(id, column_name, value);
        });

        $('#add').click(function () {
            var html = '<tr>';
            html += '<td contenteditable id="data1"></td>';
            html += '<td contenteditable id="data2"></td>';
            html += '<td contenteditable id="data3"></td>';
            html += '<td contenteditable id="data4"></td>';
            html += '<td contenteditable id="data5"></td>';
            html += '<td contenteditable id="data6"></td>';
            html += '<td contenteditable id="data7"></td>';
            html += '<td><button type="button" name="insert" id="insert" class="btn btn-success btn-xs">Insert</button></td>';
            html += '</tr>';
            $('#user_data tbody').prepend(html);
        });

        $(document).on('click', '#insert', function () {
            var mitarbeiter = $('#data1').text();
            var name = $('#data2').text();
            var vorname = $('#data3').text();
            var alter = $('#data4').text();
            var nutzername = $('#data5').text();
            var Anschrift = $('#data6').text();
            var aktiv = $('#data7').text();

            if (mitarbeiter != '' && name != '' && vorname != '' && alter != '' && nutzername != '' && Anschrift != '' && aktiv != '') {
                $.ajax({
                    url: "/assets/php/insert.php",
                    method: "POST",
                    data: {
                        mitarbeiter: mitarbeiter,
                        name: name,
                        vorname: vorname,
                        alter: alter,
                        nutzername: nutzername,
                        Anschrift: Anschrift,
                        aktiv: aktiv,
                        IBAN: IBAN
                    },
                    success: function (data) {
                        $('#alert_message').html('<div class="alert alert-success">' + data + '</div>');
                        $('#user_data').DataTable().destroy();
                        fetch_data();
                    }
                });
                setInterval(function () {
                    $('#alert_message').html('');
                }, 5000);
            }
            else {
                alert("All Fields is required");
            }
        });

        $(document).on('click', '.delete', function () {
            var id = $(this).attr("id");
            if (confirm("Are you sure you want to remove this?")) {
                $.ajax({
                    url: "/assets/php/delete.php",
                    method: "POST",
                    data: {id: id},
                    success: function (data) {
                        $('#alert_message').html('<div class="alert alert-success">' + data + '</div>');
                        $('#user_data').DataTable().destroy();
                        fetch_data();
                    }
                });
                setInterval(function () {
                    $('#alert_message').html('');
                }, 5000);
            }
        });
    });
</script>
