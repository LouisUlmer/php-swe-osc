<?php
/**
 * Created by PhpStorm.
 * User: Louis
 * Date: 02.07.2018
 * Time: 13:40
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/php/AutoloaderDB.php';
unset($user);
session_destroy();
header('Location: /');
exit();