<?php
/**
 * Created by PhpStorm.
 * User: Louis
 * Date: 10.03.2018
 * Time: 12:31
 */
session_start();

if (!function_exists('classAutoLoaderDB')) {
    /**
     * @param $class
     * @throws Exception
     */
    function classAutoLoaderDB($class)
    {
        $class = strtolower($class);
        $classFile = $_SERVER['DOCUMENT_ROOT'] . '/assets/php/Database/' . $class . '.php';

        if (is_file($classFile) && !class_exists($class)) {
            if (!include str_replace('\\', DIRECTORY_SEPARATOR, $classFile)) {
                throw new Exception("Konnte " . $class . " nicht laden.");
            }
        }

    }
}
spl_autoload_register('classAutoLoaderDB');

try {
    $obj = new User();
} catch (Exception $e) {
    echo $e->getMessage(), "\n";
}

setlocale(LC_ALL, "de_DE.utf8");

if (!isset($userModel)) {
    $userModel = new User();
}
if (!isset($eventModel)) {
    $eventModel = new Event();
}

if (isset($_SESSION['ID'])) {
    $user = $userModel->getUser($_SESSION['ID']);
}