<?php
/**
 * Created by PhpStorm.
 * User: Louis
 * Date: 12.06.2018
 * Time: 13:15
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/php/AutoloaderDB.php';

if (isset($user) and isset($_POST["id"]) and isset($_POST["column_name"]) and isset($_POST["value"])) {
    $userModel->updateColum($_POST["column_name"], $_POST["value"], $_POST["id"]);
}
