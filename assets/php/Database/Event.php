<?php
/**
 * Created by PhpStorm.
 * User: Louis
 * Date: 30.06.2018
 * Time: 18:09
 */
require_once(dirname(__FILE__) . '/' . 'Databaseconnector.php');

class Event extends Databaseconnector
{
    public function test()
    {
        $result = $this->getConnection()->query("SELECT * FROM `event`;");
        while ($user = $result->fetch_object()) {
            var_dump($user);
        }
    }

    public function createEvent($eventParameters)
    {
        $stmt = $this->getConnection()->prepare("INSERT INTO `event`(`name`, `EventtypeID`,`regStart`,`regEnd`,`start`,`end`,`maxparticipant`,`description`,`prize`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);");
        $stmt->bind_param("sssssssss", $eventParameters["name"], $eventParameters["EventtypeID"],
            $eventParameters["regStart"], $eventParameters["regEnd"], $eventParameters["start"],
            $eventParameters["end"], $eventParameters["maxparticipant"], $eventParameters["description"], $eventParameters["prize"]);
        if ($stmt->execute()) {
            return true;
        } else {
            return null;
        }
    }

    public function getEvent($id)
    {
        $stmt = $this->getConnection()->prepare("SELECT `event`.* FROM `event` WHERE LOWER(`event`.EventID) = LOWER(?);");
        $stmt->bind_param("s", $id);
        if ($stmt->execute()) {
            return $stmt->get_result()->fetch_assoc();
        }
        return null;
    }

    public function getEventParticipantCount($id)
    {
        $stmt = $this->getConnection()->prepare("
        SELECT count(DISTINCT `event_participant`.`MitgliedID`)  AS participantCount FROM `event` 
        INNER JOIN `event_participant` ON `event_participant`.EventID = `event`.EventID
        WHERE LOWER(`event`.EventID) = LOWER(?);");
        $stmt->bind_param("s", $id);
        if ($stmt->execute()) {
            return $stmt->get_result()->fetch_assoc();
        }
        return null;
    }

    public function getAllIDS()
    {
        $stmt = $this->getConnection()->prepare("SELECT EventID FROM `event` ORDER BY `event`.`start` ASC;");
        if ($stmt->execute()) {
            return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        }
        return null;
    }

    public function join($MitgliedID, $EventID, $DisciplineID)
    {
        $stmt = $this->getConnection()->prepare("INSERT INTO `event_participant`(`MitgliedID`, `EventID`,`DisciplineID`) VALUES (?, ?, ?);");
        $stmt->bind_param("sss", $MitgliedID, $EventID, $DisciplineID);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }


    public function getAll()
    {
        $stmt = $this->getConnection()->prepare("SELECT * FROM `event` ORDER BY `start` ASC;");
        if ($stmt->execute()) {
            return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        }
        return null;
    }

    public function deleteEvent($id)
    {
        $user = $this->getUser($id);
        if (!empty($user)) {
            $stmt = $this->getConnection()->prepare("DELETE FROM `event` WHERE `ID` = ?;");
            $stmt->bind_param("s", $user['ID']);
            if ($result = $stmt->execute()) {
                return true;
            }
        }
        return false;
    }
}