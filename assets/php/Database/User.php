<?php
/**
 * Created by PhpStorm.
 * User: Louis
 * Date: 01.03.2018
 * Time: 14:41
 */

require_once(dirname(__FILE__) . '/' . 'Databaseconnector.php');

class User extends Databaseconnector
{
    public function test()
    {
        $result = $this->getConnection()->query("SELECT * FROM `mitglied`;");
        while ($user = $result->fetch_object()) {
            var_dump($user);
        }
    }

    public function createUser($userParameters)
    {
        $stmt = $this->getConnection()->prepare("INSERT INTO `mitglied`(`name`, `vorname`,`alter`,`nutzername`,`password`) VALUES (?, ?, ?, ?, ?);");
        $password = $this->_createPasswordHash($userParameters['password']);
        $stmt->bind_param("sssss", $userParameters['name'], $userParameters['vorname'], $userParameters['alter'], $userParameters['nutzername'], $password);
        if ($stmt->execute()) {
            return $this->getUserByName($userParameters['nutzername']);
        } else {
            return null;
        }
    }

    public function getUser($id)
    {
        $stmt = $this->getConnection()->prepare("SELECT * FROM `mitglied` WHERE LOWER(ID) = LOWER(?);");
        $stmt->bind_param("s", $id);
        if ($stmt->execute()) {
            $res = $stmt->get_result();
            return $this->_filterPassword($res->fetch_assoc());
        }
        return null;
    }


    public function getAll()
    {
        $stmt = $this->getConnection()->prepare("SELECT * FROM `mitglied`;");
        if ($stmt->execute()) {
            return $this->_filterPassword($stmt->get_result()->fetch_all(MYSQLI_ASSOC));
        }
        return null;
    }

    public function getAllresnum()
    {
        $result = $this->getConnection()->query("SELECT * FROM `mitglied`;");
        return mysqli_num_rows($result);
    }


    public function getUserByName($nutzername)
    {
        $stmt = $this->getConnection()->prepare("SELECT * FROM `mitglied` WHERE LOWER(`nutzername`) = LOWER(?);");
        $stmt->bind_param("s", $nutzername);
        if ($stmt->execute()) {
            $res = $stmt->get_result();
            return $this->_filterPassword($res->fetch_assoc());
        }
        return null;
    }

    private function getUserByNameWithPassword($nutzername)
    {
        $stmt = $this->getConnection()->prepare("SELECT * FROM `mitglied` WHERE LOWER(`nutzername`) = LOWER(?);");
        $stmt->bind_param("s", $nutzername);
        if ($stmt->execute()) {
            $res = $stmt->get_result();
            return $res->fetch_assoc();
        }
        return null;
    }

    public function deleteUser($id)
    {
        $user = $this->getUser($id);
        if (!empty($user)) {
            $stmt = $this->getConnection()->prepare("DELETE FROM `mitglied` WHERE `ID` = ?;");
            $stmt->bind_param("s", $user['ID']);
            if ($result = $stmt->execute()) {
                return true;
            }
        }
        return false;
    }

    public function updateColum($column_name, $value, $id)
    {

        $stmt = $this->getConnection()->prepare("UPDATE `mitglied` SET ? = ? WHERE LOWER(ID) = LOWER(?);");
        $stmt->bind_param("sss", $column_name, $value, $id);
        if ($stmt->execute()) {
            $res = $stmt->get_result();
            return $this->_filterPassword($res->fetch_assoc());
        }
        return null;
    }

    public function login($nutzername, $password)
    {
        $user = $this->getUserByNameWithPassword($nutzername);
        if (!empty($user) && $this->_verifyPassword($password, $user["password"])) {
            $user = $this->_filterPassword($user);
            return $user;
        } else {
            return false;
        }
    }

    private function _createPasswordHash($password)
    {
        $options = [
            'cost' => 11,
            'salt' => $this->_createUniqueSalt()
        ];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    private function _createUniqueSalt()
    {
        return substr(strtr(base64_encode(hex2bin($this->RandomToken(32))), '+', '.'), 0, 44);
    }

    private function RandomToken($length = 32)
    {
        if (!isset($length) || intval($length) <= 8) {
            $length = 32;
        }
        if (function_exists('random_bytes')) {
            return bin2hex(random_bytes($length));
        }
        if (function_exists('mcrypt_create_iv')) {
            return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
        }
        if (function_exists('openssl_random_pseudo_bytes')) {
            return bin2hex(openssl_random_pseudo_bytes($length));
        }
    }

    private function _verifyPassword($pass, $hash)
    {
        return password_verify($pass, $hash);
    }

    private function _filterPassword($user)
    {
        unset($user['password']);
        return $user;
    }
}