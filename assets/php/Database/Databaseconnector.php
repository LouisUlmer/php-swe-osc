<?php
/**
 * Created by PhpStorm.
 * User: Louis
 * Date: 01.03.2018
 * Time: 14:28
 */

class Databaseconnector
{
    /**
     * @var
     */
    protected $_connection;

    public function setup($host, $username, $password, $database)
    {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
    }

    /**
     * @return bool|mysqli
     */
    private function connect()
    {

        if (!isset($_connection)) {
            $_connection = new mysqli($this->host, $this->username, $this->password, $this->database);
        }

        // If connection was not successful, handle the error
        if ($_connection === false) {
            // Handle error - notify administrator, log to a file, show an error screen, etc.
            return false;
        }
        return $_connection;
    }

    /**
     * @return bool|mysqli
     */
    public function getConnection()
    {
        if (!isset($_connection)) {
            $this->_connection = $this->connect();
        }
        return $this->_connection;
    }

    public function closeConnection()
    {
        $this->getConnection()->close();
    }

    /**
     * Fetch the last error from the database
     *
     * @return string Database error message
     */
    public function error()
    {
        $connection = $this->connect();
        return $connection->error;
    }

    /**
     * Quote and escape value for use in a database query
     *
     * @param string $value The value to be quoted and escaped
     * @return string The quoted and escaped string
     */
    public function quote($value)
    {
        $connection = $this->connect();
        return "'" . $connection->real_escape_string($value) . "'";
    }

    private $host = 'localhost';
    private $username = 'root';
    private $password = '';
    private $database = 'osc';
}