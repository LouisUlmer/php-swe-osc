-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 02. Jul 2018 um 00:16
-- Server-Version: 10.1.32-MariaDB
-- PHP-Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `osc`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `discipline`
--

CREATE TABLE `discipline` (
  `DisciplineID` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `discipline`
--

INSERT INTO `discipline` (`DisciplineID`, `name`, `description`) VALUES
(1, 'Oylmpische Schnellfeuerpistole', NULL),
(2, 'Sportpistole', NULL),
(3, 'Luftgewehr', NULL),
(4, 'Luftpistole', NULL),
(5, 'Zentralfeuerpistole', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `event`
--

CREATE TABLE `event` (
  `EventID` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `EventtypeID` int(11) NOT NULL,
  `regStart` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `regEnd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `maxparticipant` int(10) NOT NULL DEFAULT '0',
  `description` text,
  `prize` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `event`
--

INSERT INTO `event` (`EventID`, `name`, `EventtypeID`, `regStart`, `regEnd`, `start`, `end`, `maxparticipant`, `description`, `prize`) VALUES
(1, 'Weihnachtsschießen', 1, '2018-12-14 23:00:00', '2018-12-21 23:00:00', '2018-12-29 15:00:00', '2018-12-29 19:00:00', 54, 'Auf unseren Schießstand können sie alles bis zu 1500 Joul Schießen. Lorem ipsum dolor sit amet, ', '1. Lolli \r\n2. Osterhase\r\n3. Wheinahtsman'),
(2, '1April', 2, '2019-02-14 23:00:00', '2019-03-13 23:00:00', '2019-03-31 22:00:00', '2019-04-01 10:00:00', 3, 'Auf unseren Schießstand können sie alles bis zu 1500 Joul Schießen. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `eventtype`
--

CREATE TABLE `eventtype` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `eventtype`
--

INSERT INTO `eventtype` (`id`, `name`, `description`) VALUES
(1, 'Wettkampf', NULL),
(2, 'Organisatorisch', NULL),
(3, 'Freizeit', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `event_participant`
--

CREATE TABLE `event_participant` (
  `MitgliedID` int(10) NOT NULL,
  `EventID` int(10) NOT NULL,
  `DisciplineID` int(10) NOT NULL DEFAULT '1',
  `startnr` int(11) NOT NULL DEFAULT '1',
  `ring` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `event_participant`
--

INSERT INTO `event_participant` (`MitgliedID`, `EventID`, `DisciplineID`, `startnr`, `ring`) VALUES
(7, 1, 1, 1, 0),
(7, 2, 1, 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mitglied`
--

CREATE TABLE `mitglied` (
  `ID` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `vorname` varchar(255) NOT NULL,
  `alter` int(4) NOT NULL,
  `nutzername` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `beitrit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Anschrift` text,
  `aktiv` int(1) NOT NULL DEFAULT '1',
  `IBAN` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `mitglied`
--

INSERT INTO `mitglied` (`ID`, `name`, `vorname`, `alter`, `nutzername`, `password`, `beitrit`, `Anschrift`, `aktiv`, `IBAN`) VALUES
(6, 'Happy', 'asdas', 1992, 'asjlkdhkjasd', '$2y$11$cVNuWlFiOHprNDl2SjZTN.ADpYkuvwlMS3CXq22PYATZh1Ld2GwHu', '2018-06-15 13:12:05', NULL, 1, NULL),
(7, 'df', 'Louis', 1997, 'lulmer', '$2y$11$WU1pWTlyWTVtYWwzNGoue.5y81MRVXmvT2cZ1zx4gSsxRlPcTbTo.', '2018-06-30 18:30:19', NULL, 1, NULL);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `discipline`
--
ALTER TABLE `discipline`
  ADD PRIMARY KEY (`DisciplineID`);

--
-- Indizes für die Tabelle `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`EventID`);

--
-- Indizes für die Tabelle `eventtype`
--
ALTER TABLE `eventtype`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `event_participant`
--
ALTER TABLE `event_participant`
  ADD PRIMARY KEY (`MitgliedID`,`EventID`,`DisciplineID`);

--
-- Indizes für die Tabelle `mitglied`
--
ALTER TABLE `mitglied`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `nutzername` (`nutzername`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `discipline`
--
ALTER TABLE `discipline`
  MODIFY `DisciplineID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT für Tabelle `event`
--
ALTER TABLE `event`
  MODIFY `EventID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT für Tabelle `eventtype`
--
ALTER TABLE `eventtype`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `mitglied`
--
ALTER TABLE `mitglied`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
