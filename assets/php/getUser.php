<?php
/**
 * Created by PhpStorm.
 * User: Louis
 * Date: 14.06.2018
 * Time: 13:41
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/php/AutoloaderDB.php';

$data = [
    "login" => isset($user),
    "data" => isset($user) ? $user : "NULL"
];

header('Content-Type: application/json');
echo json_encode($data);