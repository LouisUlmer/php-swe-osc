<?php
/**
 * Created by PhpStorm.
 * User: Louis
 * Date: 14.06.2018
 * Time: 10:32
 */
if (isset($_POST["name"]) and isset($_POST["vorname"]) and isset($_POST["alter"]) and isset($_POST["nutzername"]) and isset($_POST["password"])) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/php/AutoloaderDB.php';

    if (!$userModel->getUserByName($_POST["nutzername"]) && !$User = $userModel->createUser([
            "name" => $_POST["name"],
            "vorname" => $_POST["vorname"],
            "alter" => $_POST["alter"],
            "nutzername" => $_POST["nutzername"],
            "password" => $_POST["password"]
        ])
    )
        $_SESSION["ID"] = $User["ID"];
}
header('Location: /');
exit();