<?php
/**
 * Created by PhpStorm.
 * User: Louis
 * Date: 15.06.2018
 * Time: 15:14
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/php/AutoloaderDB.php';
$_SESSION['ID'] = 7;
if (isset($_POST["nutzername"]) and isset($_POST["password"]) and !isset($user)) {
    $userlogin = $userModel->login($_POST["nutzername"], $_POST["password"]);
    if ($userlogin) {
        $user = $userlogin;
        $_SESSION["ID"] = $user["ID"];
        header('Location: /?l=t');
        exit();
    }
}

header('Location: /?l=f');
exit();