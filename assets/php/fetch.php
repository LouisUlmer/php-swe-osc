<?php
/**
 * Created by PhpStorm.
 * User: Louis
 * Date: 12.06.2018
 * Time: 13:16
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/php/AutoloaderDB.php';


$result = $userModel->getAll();
for ($i = 0; $i < count($result); ++$i)
{
    $sub_array = array();
    $sub_array[] = '<div contenteditable class="update" data-id="'.$result[$i]["ID"].'" data-column="mitarbeiter">' . $result[$i]["mitarbeiter"] . '</div>';
    $sub_array[] = '<div contenteditable class="update" data-id="'.$result[$i]["ID"].'" data-column="name">' . $result[$i]["name"] . '</div>';
    $sub_array[] = '<div contenteditable class="update" data-id="'.$result[$i]["ID"].'" data-column="vorname">' . $result[$i]["vorname"] . '</div>';
    $sub_array[] = '<div contenteditable class="update" data-id="'.$result[$i]["ID"].'" data-column="alter">' . $result[$i]["alter"] . '</div>';
    $sub_array[] = '<div contenteditable class="update" data-id="'.$result[$i]["ID"].'" data-column="nutzername">' . $result[$i]["nutzername"] . '</div>';
    $sub_array[] = '<div contenteditable class="update" data-id="'.$result[$i]["ID"].'" data-column="Anschrift">' . $result[$i]["Anschrift"] . '</div>';
    $sub_array[] = '<div contenteditable class="update" data-id="'.$result[$i]["ID"].'" data-column="aktiv">' . $result[$i]["aktiv"] . '</div>';
    $sub_array[] = '<button type="button" name="delete" class="btn btn-danger btn-xs delete" id="'.$result[$i]["ID"].'">Delete</button>';
    $data[] = $sub_array;
}



$output = array(
    "draw"    => intval(0),
    "recordsTotal"  =>  $userModel->getAllresnum(),
    "recordsFiltered" => 0,
    "data"    => $data
);
header('Content-Type: application/json');
echo json_encode($output);


