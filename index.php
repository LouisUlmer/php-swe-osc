<!DOCTYPE HTML>
<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/php/AutoloaderDB.php'; ?>
<!--
	Miniport by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<head>
    <title>OSC Schissen</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!--[if lte IE 8]>
    <script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ie8.css"/><![endif]-->
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="assets/css/ie9.css"/><![endif]-->
</head>
<body>

<!-- Nav -->
<nav id="nav">
    <ul class="container">
        <li><a href="/">Home</a></li>
        <li><a href="http://www.osc-bremerhaven.de/">OSC</a></li>
        <li><a href="./event.php">Events</a></li>
        <?php if (isset($user)) { ?>
            <li><a href="./mitgliederliste.php">Mitgliederliste</a></li>
        <?php } ?>
        <?php if (isset($user)) { ?>
            <li><a href="./essen.php">Essen</a></li>
            <li><a href="./lager.php">Lager</a></li>
        <?php } ?>
        <li>
            <a href="./<?php echo isset($user) ? "logout" : "login" ?>.php"><?php echo isset($user) ? "Logout" : "Login" ?></a>
        </li>
    </ul>
</nav>

<!-- Home -->
<div class="wrapper style1 first">
    <article class="container" id="top">
        <div class="row">
            <div class="4u 12u(mobile)">
                <span class="image fit"><img src="images/logo.png" alt=""/></span>
            </div>
            <div class="8u 12u(mobile)">
                <header>
                    <h1>OSC <strong></strong>!</h1>
                </header>
                <p>Wir die OSC Schießsportabteilung sind eine kleine Abteilung mit freundlichen und aufgeschlossenen
                    Mitgliedern. Wir kennen uns alle untereinander und würden uns über neuzugänge freuen. Schauen sie
                    sich doch einmal auf unsere Website um, es ist bestimmt etwas dabei was sie Interessiert.</p>
                <a href="./registrieren.php" class="button big scrolly">Bewerbe dich jetzt.</a>
            </div>
        </div>
    </article>
</div>

<!-- Work -->
<div class="wrapper style2">
    <article id="work">
        <header>
            <h2>Hier ist alles was du benötigst.</h2>
            <p></p>
        </header>
        <div class="container">
            <div class="row">
                <div class="4u 12u(mobile)">
                    <section class="box style1">
                        <span class="icon featured fa-crosshairs"></span>
                        <h3>Das richtige für dich.</h3>
                        <p>Sie wollen sich in Wettkämpfen oder einfach nur Spaß beim schießen haben?</p>
                    </section>
                </div>
                <div class="4u 12u(mobile)">
                    <section class="box style1">
                        <span class="icon featured fa-ticket"></span>
                        <h3>Events</h3>
                        <p>Wir Organisieren jedes jahr Veranstaltungen. Osterschießen, Weihnachtsschießen,
                            Königsschießen ... </p>
                    </section>
                </div>
                <div class="4u 12u(mobile)">
                    <section class="box style1">
                        <span class="icon featured fa-car"></span>
                        <h3>Anreise</h3>
                        <p>Höllenhammsweg 13</br>27574 Bremerhaven</p>
                    </section>
                </div>
            </div>
        </div>
        <footer>
            <p>Möchtest du an Events teielnehmen, dann melde dich jetzt an.</p>
            <a href="./login.php" class="button big scrolly">Anmelden</a>
        </footer>
    </article>
</div>

<!-- Portfolio -->
<div class="wrapper style3">
    <article id="portfolio">
        <header>
            <h2>Hier siehst du ein paar Aktuelle Sachen </h2>
            <p>WOOOOOW ein paar sachen.</p>
        </header>
        <div class="container">
            <div class="row">
                <div class="4u 12u(mobile)">
                    <article class="box style2">
                        <a href="#" class="image featured"><img src="images/medal.svg" alt=""/>


                        </a>
                        <h3><a href="#">Die Meisterschaft</a></h3>
                        <p>Vereinsmeisterschaften werden im Verein geschossen, Standort der weiteren Meisterschaften
                            Variieren jedes Jahr.</p>
                    </article>
                </div>
                <div class="4u 12u(mobile)">
                    <article class="box style2">
                        <a href="#" class="image featured"><img src="images/bazooka.svg" alt=""/></a>
                        <h3><a href="#">Waffen</a></h3>
                        <p>Auf unseren Schießstand können sie alles bis zu 1500 Joul Schießen.</p>
                    </article>
                </div>
                <div class="4u 12u(mobile)">
                    <article class="box style2">
                        <a href="#" class="image featured"><img src="images/kalashnikov-.svg" alt=""/></a>
                        <h3><a href="#">Sonstiges</a></h3>
                        <p>Hier einiges zu nicht Schießveranstaltungen</p>
                    </article>
                </div>
            </div>
        </div>
        <footer>
            <p>Möchtest du noch mehr erfahren?</p>
            <a href="#contact" class="button big scrolly">Hier ist mehr über uns</a>
        </footer>
    </article>
</div>

<!-- Contact -->
<div class="wrapper style4">
    <article id="contact" class="container 75%">
        <header>
            <h2>hast du fragen ?</h2>
            <p>DANN Frag doch endlich!!!</p>
        </header>
        <div>
            <div class="row">
                <div class="12u">
                    <form method="post" action="#">
                        <div>
                            <div class="row">
                                <div class="6u 12u(mobile)">
                                    <input type="text" name="name" id="name" placeholder="Name"/>
                                </div>
                                <div class="6u 12u(mobile)">
                                    <input type="text" name="email" id="email" placeholder="Email"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="12u">
                                    <input type="text" name="subject" id="subject" placeholder="Subject"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="12u">
                                    <textarea name="message" id="message" placeholder="Message"></textarea>
                                </div>
                            </div>
                            <div class="row 200%">
                                <div class="12u">
                                    <ul class="actions">
                                        <li><input type="submit" value="Send Message"/></li>
                                        <li><input type="reset" value="Clear Form" class="alt"/></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="12u">
                    <hr/>
                    <h3>Find me on ...</h3>
                    <ul class="social">
                        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                        <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
                        <li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
                        <li><a href="#" class="icon fa-tumblr"><span class="label">Tumblr</span></a></li>
                        <li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
                        <li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
                        <!--
                        <li><a href="#" class="icon fa-rss"><span>RSS</span></a></li>
                        <li><a href="#" class="icon fa-instagram"><span>Instagram</span></a></li>
                        <li><a href="#" class="icon fa-foursquare"><span>Foursquare</span></a></li>
                        <li><a href="#" class="icon fa-skype"><span>Skype</span></a></li>
                        <li><a href="#" class="icon fa-soundcloud"><span>Soundcloud</span></a></li>
                        <li><a href="#" class="icon fa-youtube"><span>YouTube</span></a></li>
                        <li><a href="#" class="icon fa-blogger"><span>Blogger</span></a></li>
                        <li><a href="#" class="icon fa-flickr"><span>Flickr</span></a></li>
                        <li><a href="#" class="icon fa-vimeo"><span>Vimeo</span></a></li>
                        -->
                    </ul>
                    <hr/>
                </div>
            </div>
        </div>
        <footer>
            <ul id="copyright">
                <li>&copy; Untitled. All rights reserved.</li>
                <li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
            </ul>
        </footer>
    </article>
</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.scrolly.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/skel-viewport.min.js"></script>
<script src="assets/js/util.js"></script>
<!--[if lte IE 8]>
<script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/main.js"></script>

</body>
</html>